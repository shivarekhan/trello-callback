/*Problem 1: Write a function that will return a particular board's information based on the 
boardID that is passed from the given list of boards in boards.json and then pass control back 
to the code that called it by using a callback function.*/
const boards = require('./boards.json');


function getBoardDetailsById(id, callback) {
    const randomTime = Math.random() * 10;
    if (typeof id !== 'string' || id === undefined || typeof (callback) !== 'function') {

        if (typeof callback === 'function') {
            const customError = new Error('INPUTS ID IS INVALID PLEASE ENTER VALID ID');
            callback(customError, null);
        } else {
            const customError = new Error('INPUTS ARE INVALID PLEASE ENTER VALID INPUT');
            callback(customError, null);
        }
    }else{
        setTimeout(function () {

            const boardDetails = boards.filter(function (board) {
                if (board.id === id) {
                    return true;
                }
            })
            if (boardDetails.length == 0) {
                const customError = new Error("NO board found with provided id");
                callback(customError);
            }
            else {
                callback(null, boardDetails);
            }
    
        }, randomTime * 1000);
    }

    }

    

module.exports = getBoardDetailsById;