/* 
    Problem 2: Write a function that will return all lists that belong to a board based on the 
    boardID that is passed to it from the given data in lists.json. Then pass control back to the 
    code that called it by using a callback function.
*/
const lists = require('./lists.json');

function listBelongsToBoardId(id, callback) {
    const randomTime = Math.random() * 10;

    if (id === undefined || typeof (id) !== 'string' || typeof (callback) !== 'function') {

        if (typeof callback === 'function') {
            const customError = new Error('INPUTS ID IS INVALID PLEASE ENTER VALID ID');
            callback(customError, null);
        } else {
            const customError = new Error('INPUTS ARE INVALID PLEASE ENTER VALID INPUT');
            callback(customError, null);
        }
    }
    else {
        setTimeout(function () {
            if (lists[id] !== undefined) {
                callback(null, lists[id]);
            }
            else {
                const customError = new Error("NO ENTRY FOUND");
                callback(customError, null);
            }
        }, randomTime * 1000);
    }
}

module.exports = listBelongsToBoardId;