/* 
    Problem 3: Write a function that will return all cards that belong to a particular list 
    based on the listID that is passed to it from the given data in cards.json. Then pass 
    control back to the code that called it by using a callback function.
*/

const cards = require('./cards.json');


function getAllCards(id, callback) {
    const randomTime = Math.random() * 10;
    
    if (id === undefined || typeof (id) !== 'string' || typeof (callback) !== 'function') {

        if (typeof callback === 'function') {
            const customError = new Error('INPUTS ID IS INVALID PLEASE ENTER VALID ID');
            callback(customError, null);
        } else {
            const customError = new Error('INPUTS ARE INVALID PLEASE ENTER VALID INPUT');
            callback(customError, null);
        }

    } else {
        setTimeout(function () {
            if (cards.hasOwnProperty(id)) {
                callback(null, cards[id]);
            } else {
                const customError = new Error("NO MATCHING RESULT FOUND FOR THE PROVIDED ID");
                callback(customError, null);
            }
        }, randomTime * 1000);
    }
}

module.exports = getAllCards;