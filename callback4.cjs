/* 
    Problem 4: Write a function that will use the previously written functions to get the 
    following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind list simultaneously
*/

const getBoardDetailsById = require("./callback1.cjs");
const listBelongsToBoardId = require("./callback2.cjs");
const getAllCards = require("./callback3.cjs");

function thanosBoardListAndMindList() {
    setTimeout(function () {
        const thanodId = "mcu453ed";

        getBoardDetailsById(thanodId, function (error, thanosData) {

            if (error) {
                console.error(error);
            } else {
                console.log("THANOS DATA : ")
                console.log(thanosData);

                listBelongsToBoardId(thanodId, function (error, thanosBoardList) {
                    if (error) {
                        console.error(error);
                    } else {
                        console.log("THANOS BOARD LIST : ");
                        console.log(thanosBoardList);

                        thanosBoardList.forEach(function (list) {
                            if (list.name.toLowerCase() == 'mind') {
                                getAllCards(list.id, function (error, data) {
                                    if (error) {
                                        console.error(error);
                                    } else {
                                        console.log("ALL CARDS FOR MIND LIST : ")
                                        console.log(data);
                                    }
                                })

                            }
                        })

                    }
                })
            }
        })
    })

}

module.exports = thanosBoardListAndMindList;